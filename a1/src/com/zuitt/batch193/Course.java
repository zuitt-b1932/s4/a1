package com.zuitt.batch193;

public class Course {

        public String name;
        public String description;
        public int seats;
        public double fee;
        public String startDate;
        public String endDate;
        public String instructor;

        public Course(){};

        //getters
        public String getName(){
                return this.name;
        }
        public String getDescription(){
                return this.description;
        }
        public int getSeats(){
                return this.seats;
        }
        public double getFee(){
                return this.fee;
        }
        public String getStartDate(){
                return this.startDate;
        }
        public String getEndDate(){
                return this.endDate;
        }
        public String getInstructor(){
                return this.instructor;
        }


        //setters

        public void setName(String name) {
                this.name = name;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public void setSeats(int seats) {
                this.seats = seats;
        }

        public void setFee(double fee) {
                this.fee = fee;
        }

        public void setStartDate(String startDate) {
                this.startDate = startDate;
        }

        public void setEndDate(String endDate) {
                this.endDate = endDate;
        }

        public void setInstructor(String instructor) {
                this.instructor = instructor;
        }
}
