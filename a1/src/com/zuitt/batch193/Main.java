package com.zuitt.batch193;

public class Main {

    public static void main(String[] args) {

        //Instantiate
        User myFirstUser = new User("Christian Michael", "Chapoco", 26, "Meycauayan City");

        //USER
        System.out.println("User's first name: ");
        System.out.println(myFirstUser.getFirstName());
        System.out.println("User's last name: ");
        System.out.println(myFirstUser.getLastName());
        System.out.println("User's age: ");
        System.out.println(myFirstUser.getAge());
        System.out.println("User's address: ");
        System.out.println(myFirstUser.getAddress());

        //COURSE
        System.out.println("Course's name:");
        Course firstCourse = new Course();
        firstCourse.setName("History 101");
        System.out.println(firstCourse.getName());

        System.out.println("Course's description:");
        firstCourse.setDescription("Learn History");
        System.out.println(firstCourse.getDescription());

        System.out.println("Course's seats:");
        firstCourse.setSeats(50);
        System.out.println(firstCourse.getSeats());

        System.out.println("Course's instructors first name:");
        firstCourse.setInstructor("Judy Lyn");
        System.out.println(firstCourse.getInstructor());


    }


}
