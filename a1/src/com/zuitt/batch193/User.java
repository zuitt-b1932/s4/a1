package com.zuitt.batch193;

public class User {

        public String firstName;

        public String lastName;

        public int age;

        public String address;

        public User(){};

        public User(String firstName, String lastName, int age, String address){
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
            this.address = address;
        }

        //getter for user
        public String getFirstName(){
            return this.firstName;
        }
        public String getLastName(){
            return this.lastName;
        }
        public int getAge(){
            return this.age;
        }
        public String getAddress(){
            return this.address;
        }

}
